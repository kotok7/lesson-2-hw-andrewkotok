package dicegame;

import java.util.Scanner;
import static dicegame.inInformation.printingPlayersAndScores;

public class DiceWinnerMethods {
    public static int maxDice;
    public static boolean oneMoreTime = true;

    public static int[] getPlayerScores(ThrowingNumbers throwingNumbers, int playerCount) {
        int[] score = new int[playerCount];
        for (int i = 0; i < playerCount; i++) {
            score[i] = throwingNumbers.throwTheDice();
        }
        return score;
    }

    public static void gettingMaxCount(int playerCount, int[] score) {
        for (int i = 0; i < playerCount; i++) {
            if (score[i] > maxDice) {
                maxDice = score[i];
            }
        }
    }

    public static void printingWinners(String[] playerNames, int[] score) {
        for (int i = 0; i < score.length; i++) {
            if (maxDice == score[i]) {
                System.out.println("The winner is " + playerNames[i] + " who rolled " + maxDice);
            }
        }
    }

    public static void playingOneMoreTime(ThrowingNumbers throwingNumbers, Scanner scanner, int playerCount, String[] playerNames) {
        while (oneMoreTime) {
            System.out.println("If you want to roll the Dice again, please enter 'Y': \nIf there are enough games for today - press any other button.");
            String scanner1 = scanner.nextLine();
            if (scanner1.equalsIgnoreCase("Y")) {
                int[] score1 = getPlayerScores(throwingNumbers, playerCount);
                printingPlayersAndScores(playerCount, playerNames, score1);
                maxDice = 0;
                gettingMaxCount(playerCount, score1);
                printingWinners(playerNames, score1);
            } else {
                oneMoreTime = false;
            }
        }
    }
}

