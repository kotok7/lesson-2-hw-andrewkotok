package dicegame;

import java.util.Scanner;

import static dicegame.DiceWinnerMethods.*;
import static dicegame.inInformation.*;

public class DiceMain {
    public static void main(String[] args) {
        System.out.println("Hello my friend! \nDo you wanna play a little game? >;)");
        ThrowingNumbers throwingNumbers = new ThrowingNumbers();

        Scanner scanner = new Scanner(System.in);
        inInformation information = new inInformation(scanner);
        int playerCount = information.readPlayersCount();
        String[] playerNames = getPlayerNames(scanner, playerCount);

        int[] score = getPlayerScores(throwingNumbers, playerCount);
        printingPlayersAndScores(playerCount, playerNames, score);
        gettingMaxCount(playerCount, score);
        printingWinners(playerNames, score);
        playingOneMoreTime(throwingNumbers, scanner, playerCount, playerNames);

    }
}


