package dicegame;

import lombok.RequiredArgsConstructor;

import java.util.Scanner;

@RequiredArgsConstructor
public class inInformation {
    private final Scanner scanner;

    public int readPlayersCount() {
        System.out.println("Enter the number of players: ");
        int number = scanner.nextInt();
        scanner.nextLine();
        return number;
    }

    public static String[] getPlayerNames(Scanner scanner, int playerCount) {
        String[] playerNames = new String[playerCount];
        System.out.println("Please, enter Player names: ");
        for (int i = 0; i < playerCount; i++) {
            playerNames[i] = scanner.nextLine();
        }
        return playerNames;
    }

    public static void printingPlayersAndScores(int playerCount, String[] playerNames, int[] score) {
        for (int i = 0; i < playerCount; i++) {
            System.out.println("Player " + playerNames[i] + " rolled the die and get " + score[i]);
        }
    }
}
