package dicegame;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ThrowingNumbers {
    private final int min = 1;
    private final int max = 6;

    int throwTheDice() {
        return (int) (Math.random() * (max - min)) + min;
    }
}
